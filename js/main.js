var selectedInput;
window.onload = function () {
  document
    .querySelectorAll(".time-picker")
    .forEach(item => item.addEventListener("click", openClock));
};

/**
 * This function generates all the clock numbers and sets a selected number into the face clock.
 * @param {*} id : id of the specific element to place the clock.
 * @param {*} type : sets a css class to the number element on the clock.
 * @param {*} selected : The number that will be set on the clock once is display on the element.
 */

function generateClock(id, type, selected) {
  var main = document.getElementById(id);
  removeChilds(main);
  var mainHeight = main.offsetHeight;
  var circleArray = [];
  var n = 12;
  var rx = 90;
  var ry = 86;
  var startOffset = 1;
  var multiplier = type == "hour" ? 1 : 5;

  for (var i = 0; i < n; i++) {
    var circle = document.createElement("div");
    var num = (i + 1) * multiplier;
    num = num == 60 ? 0 : num;
    var text = num == 0 ? "00" : num;
    var textNode = document.createTextNode(text);
    circle.appendChild(textNode);
    circle.className = "circle " + type;
    circle.setAttribute("data-val", num);
    circle.addEventListener("click", onClickAnalogNumber);
    circleArray.push(circle);
    circleArray[i].style.top =
      ry - ry * Math.cos(360 / n / 180 * (i + startOffset) * Math.PI) + "px";
    circleArray[i].style.left =
      rx + rx * Math.sin(360 / n / 180 * (i + startOffset) * Math.PI) + "px";
    circleArray[i].style.position = "absolute";
    circleArray[i].style.alignSelf = "center";
    circleArray[i].style.zIndex = 3;
    main.appendChild(circleArray[i]);
    setHandPicker(selected, type);
  }
}

/**
 * Opens a new instance of a clock 
 * @param {*} e event
 */

function openClock(e) {
  disableContent(e);
  var el = e.target;
  selectedInput = el;

  var time = el.value;
  var hours = document.getElementById("hours");
  var minutes = document.getElementById("minutes");
  var am = document.getElementById("am");
  var pm = document.getElementById("pm");
  var btn_ok = document.querySelector(".ok");
  var btn_cancel = document.querySelector(".cancel");

  //Sets the event listeners.
  hours.addEventListener("click", onSetHour);
  minutes.addEventListener("click", onSetMinutes);
  am.addEventListener("click", onClickMeridiem);
  pm.addEventListener("click", onClickMeridiem);
  btn_ok.addEventListener("click", onSaveTime);
  btn_cancel.addEventListener("click", closeClock);

  removeChilds(hours);
  removeChilds(minutes);

  resetSelectedTime();
  resetMeridiem();

  if (time === "") {
    hours.setAttribute("data-val", "12");
    hours.appendChild(document.createTextNode("12"));
    minutes.setAttribute("data-val", "00");
    minutes.appendChild(document.createTextNode("00"));
    pm.classList.add("m-selected");
    hours.classList.add("t-selected");
    setHour(12);
  } else {
    var inputTime = getInputTime(time);
    hours.setAttribute("data-val", inputTime.hour);
    hours.appendChild(document.createTextNode(inputTime.hour));
    minutes.setAttribute("data-val", inputTime.minutes);
    minutes.appendChild(document.createTextNode(inputTime.minutes));
    hours.classList.add("t-selected");
    setHour(inputTime.hour);

    if (inputTime.meridiem == "AM") {
      am.classList.add("m-selected");
    } else {
      pm.classList.add("m-selected");
    }
  }
  document.querySelector(".modal").classList.remove("hide");
}

/**
 * Disables interaction with the background once the modal is activated.
 * @param {*} e 
 */
function disableContent(e) {
  e.target.blur();
  document.body.style.overflow = "hidden";
  disable();
}

/**
 * Disables all the keys on the background.
 */
function disable() {
  document.onkeydown = function (e) {
    return false;
  }
}

/**
 * Enables all the keys on the background.
 */
function enable() {
  document.onkeydown = function (e) {
    return true;
  }
}

/**
 *  Resets the interaction of the content.
 */
function enableContent() {
  document.body.style.overflow = "auto";
  enable();
}

/**
 * Given a time string, returns an object with all the separeted values.
 * @param {*} time 
 */
function getInputTime(time) {
  var tArr = time.split(":");
  if (tArr.length > 1) {
    return {
      hour: tArr[0],
      minutes: tArr[1].substring(0, 2),
      meridiem: tArr[1].substring(2, tArr[1].lenght)
    };
  } else {
    return {
      hour: '12',
      minutes: '00',
      meridiem: 'PM'
    };
  }
}

/**
 * Resets the selected time on the digital clock.
 */
function resetSelectedTime() {
  const numbers = document.querySelectorAll(".t-selected");
  numbers.forEach(i => i.classList.remove("t-selected"));
}

/**
 * Resets the selected AM/PM on the digital clock.
 */
function resetMeridiem() {
  const meridiem = document.querySelectorAll(".m-selected");
  meridiem.forEach(i => i.classList.remove("m-selected"));
}

/**
 * Once the hour on the digital clock is pressed, the function sets the time on the analog clock.
 * @param {*} e 
 */
function onSetHour() {
  var el = document.getElementById("hours");
  var hour = el.getAttribute("data-val");
  resetSelectedTime();
  el.classList.add("t-selected");
  setHour(hour);
  removeMoveEvents();
}

/**
 * Once the minutes on the digital clock are pressed, the function sets the time on the analog clock.
 * @param {*} e 
 */
function onSetMinutes() {
  var el = document.getElementById("minutes");
  var minutes = el.getAttribute("data-val");
  resetSelectedTime();
  el.classList.add("t-selected");
  setMinutes(minutes);
  setupMoveEvents();
}
/**
 * Sets the AM/PM
 * @param {*} e 
 */
function onClickMeridiem(e) {
  resetMeridiem();
  e.target.classList.add("m-selected");
}

/**
 * Once a number on the analog clock is press, the functions detects de class of the element an sets the time to the digital clock on the upper part of the component.
 * @param {*} e 
 */
function onClickAnalogNumber(e) {
  var el = e.target;
  var hour = document.getElementById("hours");
  var minutes = document.getElementById("minutes");

  // Removes previous selected numbers on the analog clock.
  const circles = document.querySelectorAll(".on");
  circles.forEach(c => c.classList.remove("on"));

  var val = el.getAttribute("data-val");

  if (el.classList.contains("hour")) {
    //Set the hand picker
    setHandPicker(val, "hour");
    // Reset the time value on the digital clock
    removeChilds(hour);
    hour.appendChild(document.createTextNode(val));
    hour.setAttribute("data-val", val);
    onSetMinutes();

  } else {
    //Set the hand picker
    setHandPicker(val, "minute");
    // Reset the time value on the digital clock
    removeChilds(minutes);
    val = val < 10 ? "0" + val : val;
    minutes.appendChild(document.createTextNode(val));
    minutes.setAttribute("data-val", val);
  }
}

/**
 * Sets the analog clock to the given hour.
 * @param {*} hour 
 */
function setHour(hour) {
  generateClock("numbers", "hour", hour);
  setHandPicker(hour, "hour");
}

/**
 * Sets the analog clock to the given minutes.
 * @param {*} min 
 */
function setMinutes(min) {
  generateClock("numbers", "minute", min);
  setHandPicker(min, "minute");
}

/**
 * Sets the hand of the analog clock to the number position.
 * @param {*} number 
 * @param {*} min 
 */
function setHandPicker(number, type) {
  var d = number;

  if ((type == "hour" && number == 12) || (type == "minute" && number == 60)) {
    d = 0;
  }

  var divider = type !== "minute" ? 12 : 60;
  const degree = d / divider * 360 + 90 + 180;

  var pickerHand = document.querySelector(".picker-hand");

  // Removes the effect to avoid wierd rotation effect.
  if (d == 0) {
    pickerHand.classList.remove('effect');
    pickerHand.style.transform = 'rotate(270deg)';
  } else {
    pickerHand.classList.add('effect');
  }

  pickerHand.style.transform = `rotate(${degree}deg)`;

  var list = document.querySelectorAll("." + type);
  list.forEach(element => {
    if (parseInt(element.getAttribute("data-val")) == number) {
      element.classList.add("on");
    }
  });
}

/**
 * Sets the time to the selected input and closes the modal clock.
 */
function onSaveTime() {
  var hours = document.getElementById("hours").getAttribute("data-val");
  var minutes = document.getElementById("minutes").getAttribute("data-val");
  minutes = minutes == 0 ? "00" : minutes;
  var meridiem = document.querySelector(".m-selected").getAttribute("data-val");
  selectedInput.value = hours + ":" + minutes + meridiem;
  enableContent();
  removeMoveEvents();
  closeClock();
}
/**
 * Closes the modal clock.
 */
function closeClock() {
  removeMoveEvents();
  document.querySelector(".modal").classList.add("hide");
}

/**
 * Deletes all the content of a given element.
 * @param {*} el 
 */
function removeChilds(el) {
  while (el.hasChildNodes()) {
    el.removeChild(el.firstChild);
  }
}

/**
 * Sets the mouse / touch to move the hand of the analog clock.
 */

function setupMoveEvents() {
  var el = document.querySelector(".face");
  el.addEventListener("mousedown", onMouseDown);
  el.addEventListener("mouseup", onMouseUp);
  el.addEventListener("touchstart", onTouchStart);
  el.removeEventListener("touchend", onTouchEnd);
}

/**
 * Removes all the event listeners to avoid movement.
 */
function removeMoveEvents() {
  var el = document.querySelector(".face");

  el.removeEventListener("mousedown", onMouseDown);
  el.removeEventListener("mouseup", onMouseUp);
  el.removeEventListener("mousemove", onMouseMove);

  el.removeEventListener("touchstart", onTouchStart);
  el.removeEventListener("touchend", onTouchEnd);
  el.removeEventListener("touchmove", onMoveTouch);
}

function onMouseDown(e) {
  var el = document.querySelector(".face");
  el.addEventListener("mousemove", onMouseMove);
}

function onMouseUp(e) {
  var el = document.querySelector(".face");
  el.removeEventListener("mousemove", onMouseMove);
}

function onTouchStart(e) {
  var el = document.querySelector(".face");
  el.addEventListener("touchmove", onMoveTouch);
}

function onTouchEnd(e) {
  var el = document.querySelector(".face");
  el.removeEventListener("touchmove", onMoveTouch);
}

/**
 * Moves the hand of the analog clock according to the mouse event of the mobile device.
 * @param {*} e as an event.
 */
function onMouseMove(e) {
  var el = document.querySelector(".face");
  var d, x, y;
  var centerX = el.offsetLeft + el.clientWidth / 2;
  var centerY = el.offsetTop + el.clientHeight / 2;
  var startAngle = 0;

  x = e.clientX - centerX;
  y = e.clientY - centerY;
  d = 180 / Math.PI * Math.atan2(y, x);
  var rotation = d - startAngle + 360;
  rotation = rotation >= 180 && rotation < 270 ? rotation + 360 : rotation;
  var pickerHand = document.querySelector(".picker-hand");
  var start = 270;
  var val = Math.round(((rotation - start) / 60) * 10);
  pickerHand.style.transform = `rotate(${rotation}deg)`;

  val = val == 60 ? 0 : val;
  val = val < 10 ? "0" + val : val;

  // Removes the effect to avoid wierd rotation effect.
  if ((rotation > 628) && (rotation < 630)) {
    pickerHand.classList.remove('effect');
    pickerHand.style.transform = 'rotate(270deg)';
  } else {
    pickerHand.classList.add('effect');
  }

  setAllMinutes(val);
}

/**
 * Moves the hand of the analog clock according to the touch event of the mobile device.
 * @param {*} e touch event
 */
function onMoveTouch(e) {
  var el = document.querySelector(".face");
  var touches = e.changedTouches;
  var touch = touches[0];
  var d, x, y;
  var centerX = el.offsetLeft + el.clientWidth / 2;
  var centerY = el.offsetTop + el.clientHeight / 2;
  var startAngle = 0;

  x = touch.clientX - centerX;
  y = touch.clientY - centerY;
  d = 180 / Math.PI * Math.atan2(y, x);
  var rotation = d - startAngle + 360;
  rotation = rotation >= 180 && rotation < 270 ? rotation + 360 : rotation;
  rotation = Math.round(rotation);
  var pickerHand = document.querySelector(".picker-hand");
  var start = 270;
  var val = Math.round(((rotation - start) / 60) * 10);
  pickerHand.style.transform = `rotate(${rotation}deg)`;

  val = val == 60 ? 0 : val;
  val = val < 10 ? "0" + val : val;

  // Removes the effect to avoid wierd rotation effect.
  if ((rotation > 628) && (rotation < 630)) {
    pickerHand.classList.remove('effect');
    pickerHand.style.transform = 'rotate(270deg)';
  } else {
    pickerHand.classList.add('effect');
  }

  setAllMinutes(val);
}

/**
 * Sets the minutes on the digital and analog clock with the given value.
 * @param {*} val 
 */
function setAllMinutes(val) {
  var minutes = document.getElementById("minutes");

  // Reset the time value on the digital clock
  removeChilds(minutes);
  minutes.appendChild(document.createTextNode(val));
  minutes.setAttribute("data-val", val);

  // Removes previous selected numbers on the analog clock.
  const circles = document.querySelectorAll(".on");
  circles.forEach(c => c.classList.remove("on"));

  var list = document.querySelectorAll(".minute");

  list.forEach(element => {
    if (parseInt(element.getAttribute("data-val")) == val) {
      element.classList.add("on");
    }
  });
}